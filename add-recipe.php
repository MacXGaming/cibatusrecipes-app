<?php
include_once 'lib/config.php';

if(isset($_POST['add'])){
    $title = $_POST['title'];
    $ingredients = $_POST['attributevalue'];
    $description = $_POST['description'];
    $note = $_POST['note'];
    $categories = $_POST['categories'];
    $tags = explode(",", strtolower($_POST['tags']));


    if(empty($title)){
        Alert::addMessage(_("What are you cooking? Tell me in the title please!"));
    }
    if(count($ingredients)==0){
        Alert::addMessage(_("A big bawl of air? Don\'t you need some ingredients?"));
    }
    if(count($categories)==0){
        Alert::addMessage(_("All disces need a category!"));
    }
    foreach ($categories as $category) {
        if(!$Recipes->existCategory($category, $userId)){
            $wrongCat = 1;
        }
    }
    if($wrongCat==1){
        Alert::addMessage(_("One or more categories doesn\'t exist!"));
    }

    if(empty(Alert::getMessages())){
        $Recipes->add($userId, $title, $ingredients, $description, $note, $categories, $tags);
        Alert::addMessage(_("Your recipe is now added!"));
        Alert::setAlert("success");
        header("location:/");
        exit();
    }else{
        Alert::setAlert("danger");
    }

}


include_once 'header.php';
?>
<div class="content">
    <div class="col-12">
        <div class="new-recipe">
            <div class="col-6">
                <form method="post" id="form">
                    <h1><input type="text" tabindex="1" name="title" class="title noEnterSubmit" placeholder="<?php echo _("Title"); ?>"></h1>
                    <div class="ingredients-list form-group">
                        <label for="arrtibutevalue"><?php echo _("Add Ingredient"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="top" data-content="<?php echo _("Type one ingredient at a time, with the amount and press the tick.<br>The ingredients will be listed from first to last added ingredient.<br>If there's a mistake or an ingredient need a correction, add the corrected ingredient to the list, move it using the arrows, and delete the old one pressing the cross.<br><b>Start with a # and it will be a headline</b><br><span style='font-size:0.7rem;'>eg. 50 g chopped nuts, of choice</span>"); ?> "></i></label>
                        <div class="attline">
                            <div class="attval"><input tabindex="2" type="text" name="attval[]" id="arrtibutevalue" class="noEnterSubmit"  value=""></div>
                            <div class="attvaladdl"><a href="javascript:void(0);" onclick="pladdline()" id="attlink"><i class="fa fa-check"></i></a></div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="ingredients-list form-group">
                        <label><?php echo _("Ingredients List"); ?></label>
                        <ul class="ingredients" id="ingredients"></ul>
                        <div class="clear"></div>
                    </div>
                    <div class="form-group">
                        <label for="description"><?php echo _("Description"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="top" data-content="<?php echo _("Describe how to make the food.<br><span style='font-size:0.7rem;'>eg. Whip the eggs before slowly adding them to the mixture.<br>Baked for 20 mins. at 180 degrees.<br>Make sure the cake has been in the oven for at least 20 mins before opening. It can collapse!</span>"); ?>"></i></label>
                        <textarea tabindex="3" rows="10" id="description" name="description" class="text-field form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="note"><?php echo _("Note"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="top" data-content="<?php echo _("Note things you want to remember with this recipe.<br><span style='font-size:0.7rem;'>eg. Works well, served with sour cream.<br>Martin enjoys this very much.</span>"); ?>"></i></label>
                        <textarea tabindex="4" rows="10" id="note" name="note" class="text-field form-control"></textarea>
                    </div>
                    <div class="category-list form-group">
                        <label for="selectCategories"><?php echo _("Categories"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="top" data-content="<?php echo _("Choose one or more categories for the recipe. This will help finding the recipe again, even if you forget the title you give it."); ?>"></i></label>
                        <select tabindex="5" multiple id="selectCategories" name="categories[]" class="form-control" style="height:200px;" id="countries" data-usesprite="smallIcons">
                            <?php
                            foreach ($Recipes->getCategories($userId) as $category) {
                                $icon = $Icon->get($category->iconId);
                                echo "<option value=\"{$category->id}\" data-icon=\"{$icon->class}\">{$category->name}</option>\n";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="tags form-group">
                        <label for="tags"><?php echo _("Tags"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="top" data-content="<?php echo _("Adding tags to a recipe, can make it easier to find it again. It can be ingredients, allergy-prone ingredients or the appropriate celebration, in which it's served.<br><span style='font-size:0.7rem;'>eg. christmas, nuts, gluten, chocolate</span>"); ?>"></i></label>
                        <input data-tab="6" type="text" data-id="tags" name="tags" class="tags-input" value="" data-role="tagsinput">
                    </div>
                    <div class="form-group">
                        <input tabindex="7" id="submit" type="submit" name="add" class="noEnterSubmit btn btn-success" value="<?php echo _("Add Recipe"); ?>">
                    </div>
                    <div class="clear"></div>
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<script src="/js/bootstrap-tagsinput.js"></script>
<script src="/js/jquery.multi-select.js"></script>
<script src="/js/jquery.select.js"></script>
<script>
$(document).ready(function() {
    $('.noEnterSubmit').keypress(function(e){
        if ( e.which == 13 && !$("#submit").is(":focus")) e.preventDefault();
    });
    $('#selectCategories').multiSelect({
        keepOrder: true,
        selectableHeader: "<div style='text-align:center;font-weight:bold;font-size:12px;'><?php echo _("Not Choosen"); ?></div>",
        selectionHeader: "<div style='text-align:center;font-weight:bold;font-size:12px;'><?php echo _("Choosen"); ?></div>"
    });


    $('[data-toggle="popover"]').popover({html:true});
    $('#attlink').hide();

    $('#arrtibutevalue').keydown(function (e){
        if(e.keyCode == 13){
            pladdline();
        }
    });
    $('#arrtibutevalue').keyup(function( event ) {
        if($('#arrtibutevalue').val().length>0){
            $('#attlink').fadeIn();
        }
        if($('#arrtibutevalue').val().length===0){
            $('#attlink').fadeOut();
        }
    });
});
function pladdline(){
    var err='';
    var title='';

    if($('#arrtibutevalue').val()==="") {

    }else{
        var pList='<li><input type="hidden" name="attributevalue[]" value="'+$('#arrtibutevalue').val()+'"><input disabled class="attributevalue" value="'+$('#arrtibutevalue').val()+'"><a href="#"><i class="fa fa-arrows-v"></i></a><a href="javascript:void(0);" onclick="$(this).parent().remove();"><i class="fa fa-times"></i></a></li>';
        $('#ingredients').append(pList);
    }
    $('#arrtibutevalue').val('');
    $('#attlink').fadeOut();
}

$('#ingredients').sortable();
</script>
<?php
include_once 'footer.php';
?>
