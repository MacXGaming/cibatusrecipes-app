<?php
include_once 'header.php';
?>
<div class="content">
    <div class="col-12 recipe">
        <div class="form-group">
            <labal>Navigation & Header Background</labal>
            <div class="input-group">
                <input id="NavHeads" class="jscolor {onFineChange:'updateNavHeads(this)'} form-control" value="222831">
                <span class="input-group-btn">
                    <button onclick="resetNavHeads();" class="btn btn-default">Reset</button>
                </span>
            </div>
        </div>
        <div class="form-group">
            <labal>Navigation Header Color</labal>
            <div class="input-group">
                <input id="NavHeadsFont" class="jscolor {onFineChange:'updateNavHeadsFont(this)'} form-control" value="EEEEEE">
                <span class="input-group-btn">
                    <button onclick="resetNavHeadsFont();" class="btn btn-default">Reset</button>
                </span>
            </div>
        </div>
        <div class="form-group">
            <labal>Categories Bar</labal>
            <div class="input-group">
                <input id="CatBar" class="jscolor {onFineChange:'updateCatBar(this)'} form-control" value="222831">
                <span class="input-group-btn">
                    <button onclick="resetCatBar();" class="btn btn-default">Reset</button>
                </span>
            </div>
        </div>
        <div class="form-group">
            <labal>Categories Font Color</labal>
            <div class="input-group">
                <input id="CatBarFont" class="jscolor {onFineChange:'updateCatBarFont(this)'} form-control" value="EEEEEE">
                <span class="input-group-btn">
                    <button onclick="resetCatBarFont();" class="btn btn-default">Reset</button>
                </span>
            </div>
        </div>
        <div class="form-group">
            <labal>Nav Background</labal>
            <div class="input-group">
                <input id="NavBackground" class="jscolor {onFineChange:'updateNavBackground(this)'} form-control" value="C9C9C9">
                <span class="input-group-btn">
                    <button onclick="resetNavBackground();" class="btn btn-default">Reset</button>
                </span>
            </div>
        </div>
        <div class="form-group">
            <labal>Header Background</labal>
            <div class="input-group">
                <input id="HeaderBackground" class="jscolor {onFineChange:'updateHeaderBackground(this)'} form-control" value="C9C9C9">
                <span class="input-group-btn">
                    <button onclick="resetHeaderBackground();" class="btn btn-default">Reset</button>
                </span>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<script>
function resetNavHeads(){
    $('#mySidenav h1').css('background', '#222831');
    document.getElementById('NavHeads').jscolor.fromString('222831')
    localStorage.removeItem("NavHeads");
}
function resetNavHeadsFont(){
    $('#mySidenav h1').css('color', '#EEEEEE');
    document.getElementById('NavHeadsFont').jscolor.fromString('EEEEEE')
    localStorage.removeItem("NavHeadsFont");
}
function resetCatBar(){
    $('.categories').css('background', '#222831');
    document.getElementById('CatBar').jscolor.fromString('222831')
    localStorage.removeItem("CatBar");
}
function resetCatBarFont(){
    $('.categories').css('color', '#EEEEEE');
    $('.categories i').css('color', '#EEEEEE');
    document.getElementById('CatBarFont').jscolor.fromString('EEEEEE')
    localStorage.removeItem("CatBarFont");
}
function resetNavBackground(){
    $('.sidenav').css('background', '#C9C9C9');
    document.getElementById('NavBackground').jscolor.fromString('C9C9C9')
    localStorage.removeItem("NavBackground");
}
function resetHeaderBackground(){
    $('#main .header').css('background', '#C9C9C9');
    document.getElementById('HeaderBackground').jscolor.fromString('C9C9C9')
    localStorage.removeItem("HeaderBackground");
}

function updateNavHeads(jscolor) {
    $('#mySidenav h1').css('background', '#'+jscolor);
    localStorage.setItem("NavHeads", '#'+jscolor);
}
function updateNavHeadsFont(jscolor) {
    $('#mySidenav h1').css('color', '#'+jscolor);
    localStorage.setItem("NavHeadsFont", '#'+jscolor);
}
function updateCatBar(jscolor) {
    $('.categories').css('background', '#'+jscolor);
    localStorage.setItem("CatBar", '#'+jscolor);
}
function updateCatBarFont(jscolor) {
    $('.categories').css('color', '#'+jscolor);
    $('.categories i').css('color', '#'+jscolor);
    localStorage.setItem("CatBarFont", '#'+jscolor);
}
function updateNavBackground(jscolor) {
    $('.sidenav').css('background', '#'+jscolor);
    localStorage.setItem("NavBackground", '#'+jscolor);
}
function updateHeaderBackground(jscolor) {
    $('#main .header').css('background', '#'+jscolor);
    localStorage.setItem("HeaderBackground", '#'+jscolor);
}
</script>
<script src="/js/jscolor.js"></script>
<?php
include_once 'footer.php';
?>
