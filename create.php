<?php
include_once 'lib/config.php';
if($_SESSION['userId']){
    header("location:/");
}
if(isset($_POST['create'])){
    $name=$_POST['name'];
    $email=$_POST['email'];
    $password=$Account->c_hash($_POST['password']);
    $repeat_password=$Account->c_hash($_POST['repeat-password']);
    $language=$_POST['language'];

    if(empty($name)){
        $errors[] = _("Don\'t you have a name?");
    }
    if(empty($email)){
        $errors[] = _("How can we contact you?");
    }
    if(empty($language)){
        $errors[] = _("You have to choose a primary language?");
    }
    if(empty($password)){
        $errors[] = _("You have to login with something?");
    }
    if($password!=$repeat_password){
        $errors[] = _("Your passwords doesn\'t match!");
    }
    if($Account->existAccountFromEmail($email)){
        $errors[] = _("Your already have an account!");
    }

    if(empty($errors)){
        $Account->createAccount($name, $email, $password, $language);
        Alert::addAlert("success", array(_("You created an account! Check your email for activation!")));
        header("location:/");
        exit();
    }else{
        Alert::addAlert("danger", $errors);
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <base href="http://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo _("Create Account"); ?> | <?php echo _("CibatusRecipes"); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="style/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-page skin-red">
    <div class="login-box">
        <div class="login-logo">
            <!-- <a href="/"><img src="/landing/img/logos/logo-lg.png"></a> -->
        </div>
        <div class="login-box-body">
            <p class="login-box-msg"><?php echo _("Create an account to login!"); ?></p>
            <form action="" method="post">
                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="<?php echo _("Name"); ?>">
                </div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="<?php echo _("Email"); ?>">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="<?php echo _("Password"); ?>">
                </div>
                <div class="form-group">
                    <input type="password" name="repeat-password" class="form-control" placeholder="<?php echo _("Repeat Password"); ?>">
                </div>
                <div class="form-group">
                    <select name="language" class="form-control form-select" style="border-radius: 0; -webkit-appearance: none; -moz-appearance: none;">
                        <option value=""><?php echo _("Choose primary language"); ?></option>
                        <option value="DK">Danish</option>
                        <option value="US">English</option>
                    </select>
                </div>
                <div class="col-8">
                    <a href="/login"><?php echo _("Back to login"); ?></a><br>
                </div>
                <div class="col-4">
                    <button type="submit" name="create" class="full-width btn btn-primary btn-block btn-flat"><?php echo _("Create"); ?></button>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>

    <script src="js/jquery-1.12.4.js"></script>
    <?php
    $data .= "<div class=\"alert alert-info\" role=\"alert\"><span class=\"alert_text\">$message</span></div>\n";
    $data .= "<div class=\"alert alert-success\" role=\"alert\"><span class=\"alert_text\">$message</span></div>\n";
    $data .= "<div class=\"alert alert-warning\" role=\"alert\"><span class=\"alert_text\">$message</span></div>\n";
    $data .= "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"alert_text\">$message</span></div>\n";
    echo $data;
    if (!empty($_SESSION['data_alert'])) {
        echo "<script type=\"text/javascript\">\n";
        echo "  $(document).ready(function() {\n";
            echo $_SESSION['data_alert'];
            echo "   $.ajax({\n";
                echo "     type: 'POST',\n";
                echo "     data: 'this=data_alert',\n";
                echo "     url: '/alertme',\n";
                echo "     cache: false,\n";
                echo "     async: false\n";
                echo "   });\n";
                echo "  });\n";
                echo "</script>\n";
            }
            ?>
        </body>
        </html>
