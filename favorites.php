<?php
include_once 'header.php';
?>
<div class="content homepage">
    <div class="col-12 recipe" style="position:relative;">
        <h1><?php echo _("Favorites"); ?></h1>
    </div>
    <div class="col-12 recipe-list">
        <?php
        echo "<div class=\"list\">\n";
        for ($i=0; $i <= 50; $i++) {
            echo "<div class=\"recipe\">\n";
            echo "    <h1><a href=\"/recipe/1\">Døds Pandekager</a></h1>\n";
            echo "    <div class=\"recipe-list\">\n";
            echo "        <ul>\n";
            echo "            <li>Løg</li>\n";
            echo "            <li>Is</li>\n";
            echo "            <li>Eddike</li>\n";
            echo "            <li>Citron</li>\n";
            echo "        </ul>\n";
            echo "    </div>\n";
            echo "    <div class=\"tags\">\n";
            echo "        <label>"._("Tags").":</label>\n";
            echo "        <span><a href=\"#\">Pandekager</a>, <a href=\"#\">Død</a></span>\n";
            echo "        <div class=\"clear\"></div>\n";
            echo "    </div>\n";
            echo "    <div class=\"tags\">\n";
            echo "        <label>"._("Categories").":</label>\n";
            echo "        <span><a href=\"#\">Is</a>, <a href=\"#\">Pandekager</a></span>\n";
            echo "        <div class=\"clear\"></div>\n";
            echo "    </div>\n";
            echo "</div>\n";
        }
        echo "</div>\n";
        ?>
        <div class="clear"></div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('.list').pinterest_grid({
        no_columns: 2,
        padding_x: 10,
        padding_y: 10,
        margin_bottom: 50,
        single_column_breakpoint: 1024
    });
});
</script>
<?php
include_once 'footer.php';
?>
