<script>
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

if(localStorage.getItem("NavHeads")){
    $('#mySidenav h1').css('background', localStorage.getItem("NavHeads"));
    $('#NavHeads').val(localStorage.getItem("NavHeads"));
}

if(localStorage.getItem("NavHeadsFont")){
    $('#mySidenav h1').css('color', localStorage.getItem("NavHeadsFont"));
    $('#NavHeadsFont').val(localStorage.getItem("NavHeadsFont"));
}

if(localStorage.getItem("CatBar")){
    $('.categories').css('background', localStorage.getItem("CatBar"));
    $('#CatBar').val(localStorage.getItem("CatBar"));
}

if(localStorage.getItem("CatBarFont")){
    $('.categories').css('color', localStorage.getItem("CatBarFont"));
    $('.categories i').css('color', localStorage.getItem("CatBarFont"));
    $('#CatBarFont').val(localStorage.getItem("CatBarFont"));
}

if(localStorage.getItem("NavBackground")){
    $('.sidenav').css('background', localStorage.getItem("NavBackground"));
    $('#NavBackground').val(localStorage.getItem("NavBackground"));
}

if(localStorage.getItem("HeaderBackground")){
    $('#main .header').css('background', localStorage.getItem("HeaderBackground"));
    $('#HeaderBackground').val(localStorage.getItem("HeaderBackground"));
}

</script>

<?php
echo Alert::printAlerts();
?>
</body>
</html>
