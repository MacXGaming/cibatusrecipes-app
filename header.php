<?php
include_once 'lib/config.php';
if(!isset($index)){
    $index=false;
}

if(!isset($_SESSION['userId']) AND current_page_name()!="share.php"){
    header("location:/login");
    exit();
}

?>
<!DOCTYPE html>
<html>
<head>
    <base href="<?php echo $_SERVER['HTTP_HOST']; ?>"/>
    <meta charset="utf-8">
    <title>CibatusRecipes</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" type="text/css" href="/style/css/style.css">
    <link rel="stylesheet" type="text/css" href="/style/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/style/css/font-awesome.css">
    <script src="/js/jquery-1.12.4.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/pinterest_grid.js"></script>
    <script src="/js/navigation.js"></script>
    <?php if($_SERVER['HTTP_HOST']!="localhost"){ ?>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-48801400-11', 'auto');
        ga('send', 'pageview');

        </script>
        <?php } ?>
    </head>
    <body class="">

        <div id="mySidenav" class="sidenav">
            <div class="header">
                <div class="profile-picture" style=""></div>
                <div class="btn-group">
                    <p class="dropdown-toggle" data-toggle="dropdown">René <span class="caret"></p>
                        <ul style="left:-50px;" class="dropdown-menu">
                            <li><a href="/settings/profile" style="padding-top:9px;"><?php echo _("Change profile picture"); ?></a></li>
                            <li><a href="/settings/categories" style="padding-top:9px;"><?php echo _("Edit categories"); ?></a></li>
                            <li><a href="/settings/account" style="padding-top:9px;"><?php echo _("Settings"); ?></a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/logout" style="padding-top:9px;"><?php echo _("Logout"); ?></a></li>
                        </ul>
                    </div>
                </div>
                <div class="divider"></div>
                <nav class="navigation">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li><a href="#">My Recipes</a></li>
                    </ul>
                </nav>
                <div class="divider"></div>
                <div class="personal">
                    <h1><?php echo _("Latest 3 Favorites"); ?></h1>
                    <div class="content">
                        <ul>
                            <?php
                            echo "<li><a href=\"/recipe/1\">OMG Kage</a></li>\n";
                            echo "<li><a href=\"/recipe/1\">VILDESTE Kage</a></li>\n";
                            echo "<li><a href=\"/recipe/1\">Gift Piller</a></li>\n";
                            echo "<li class=\"see-more\"><a href=\"/favorites\">"._("See more")."</a></li>\n";
                            ?>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="shared">
                    <h1><?php echo _("Latest 3 Shared"); ?></h1>
                    <div class="content">
                        <ul>
                            <?php
                            echo "<li><a href=\"/shared/1\">Døds Pandekager</a></li>\n";
                            echo "<li><a href=\"/shared/1\">Snegle Gift</a></li>\n";
                            echo "<li><a href=\"/shared/1\">Sove Kage</a></li>\n";
                            echo "<li class=\"see-more\"><a href=\"/shared\">"._("See more")."</a></li>\n";
                            ?>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>
    </div>
    <div id="main">
        <div class="header">
            <div class="menu">
                <a href="javascript:void(0)" id="closenav" style="display:none;" onclick="closeNav()"><i class="fa fa-bars"></i></a>
                <a href="javascript:void(0)" id="opennav" onclick="openNav()"><i class="fa fa-bars"></i></a>
            </div>
            <a href="/" class="logo">
                <img src="/images/logo.png">
            </a>

            <div class="categories">
                <div class="center">
                    <?php
                    foreach ($Recipes->getCategories($userId) as $category) {
                        $icon = $Icon->get($category->iconId);
                        echo "<div class=\"list-group-item\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"{$category->name}\">";
                        echo "<a href=\"/category/{$category->slug}\"><i class=\"icon icon-{$icon->class}\"></i></a>";
                        echo "</div>";
                    }
                    ?>

                    <div class="btn-group" style="margin-left:30px;">
                        <i class="icon-loop dropdown-toggle" data-toggle="dropdown"></i>
                        <div class="dropdown-menu" style="left:-12px;top:27px;padding:5px;">
                            <form method="get" action="/recipe/search/">
                                <div class="input-group">
                                    <input class="form-control" name="q" value="<?php echo (isset($_GET['q']) ? $_GET['q'] : ""); ?>" placeholder="<?php echo _("Search..."); ?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                                <div class="full-width">
                                    <button name="recipeSearchReset" class="btn btn-default form-control full-width" type="submit"><?php echo _("Reset"); ?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
