<?php
class Account{
    private static $_instance = null;

    private $DB;

    public function __construct(){
        $this->DB = DB::getInstance();
    }
    public static function getInstance() {
        if(!isset(self::$_instance)) {
            self::$_instance = new Account();
        }
        return self::$_instance;
    }


    public function createAccount($name, $email, $password, $language){
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->DB->query("INSERT INTO users (name, email, password, language, ip, type)VALUES(?, ?, ?, ?, ?, ?)", array($name, $email, $password, $language, $ip, 0))->results();
        return true;
    }


    public function changeLastOnline($id){
        $this->DB->query("UPDATE users SET last_online = ? WHERE id = ?", array(date("Y-m-d H:i:s"), $id))->results();
        return true;
    }

    /**
    *
    * Get Account from Login information
    *
    * @param string $email  contains the users email
    * @param string $password  contains the users password
    * @return object
    */
    public function getAccountFromLogin($email, $password){
        $query = $this->DB->query("SELECT * FROM users WHERE email = ? AND password = ? AND type IN (1,2)", array($email, $password))->results();
        if(!empty($query)){
            return $query;
        }else{
            return false;
        }
    }

    /**
    *
    * Get Account from ID
    *
    * @param int $id  contains the users unique id
    * @return object
    */
    public function getAccountFromID($id){
        $query = $this->DB->query("SELECT * FROM users WHERE id = ? AND type IN (1,2)", array($id))->results();
        if(!empty($query)){
            return $query;
        }else{
            return false;
        }
    }

    /**
    *
    * Get Account from Email
    *
    * @param int $id  contains the users email
    * @return object
    */
    public function getAccountFromEmail($email){
        $query = $this->DB->query("SELECT * FROM users WHERE email = ? AND type IN (1,2)", array($email))->results();
        if(!empty($query)){
            return $query;
        }else{
            return false;
        }
    }

    /**
    *
    * Get Account from email
    *
    * @param int $email  contains the users email
    * @return object
    */
    public function existAccountFromEmail($email){
        $this->DB->query("SELECT * FROM users WHERE email = ?", array($email))->results();
        if($this->DB->count()>0){
            return true;
        }else{
            return false;
        }
    }

    /**
    *
    * Update User info
    *
    * @param int $userId  contains the users unique id
    * @param string $name  contains the users name
    * @param string $email  contains the users email
    * @return boolean
    */
    public function updateAccount($userId, $name, $email, $language){
        if(empty($userId) OR empty($name) OR empty($email) OR empty($language)){
            return false;
        }
        $this->DB->query("UPDATE users SET name = ?, email = ?, language = ? WHERE id = ?", array($name, $email, $language, $userId));
        return true;
    }

    /**
    *
    * Update User password
    *
    * @param int $userId  contains the users unique id
    * @param string $password  contains the unhashed password
    * @return boolean
    */
    public function updatePassword($userId, $password){
        if(empty($userId) OR empty($password)){
            return false;
        }
        $this->DB->query("UPDATE users SET password = ? WHERE id = ?", array($this->c_hash($password), $userId));
        return true;
    }

    /**
    *
    * Update Users profile picture
    *
    * @param int $userId  contains the users unique id
    * @param array $message  contains the picture name
    * @return boolean
    */
    public function updatePicture($userId, $path){
        if(empty($userId) OR empty($path)){
            return false;
        }
        $this->DB->query("UPDATE users SET picture = ? WHERE id = ?", array($path, $userId));
        return true;
    }

    /**
    *
    * Hash String
    *
    * @param string $string  contains the unhashed string
    * @return string
    */
    public function c_hash($string="") {
        $salt = "id160cd93ccd8b3bdb8e38c1dbe76f2a7c0371c46741b5a98d0b6074b192b9983195148abf077a5b802d803ec16217474476ef335303cb73a0583d3243d84213";
        $hash = hash('sha512', $string . $salt);
        for ($i = 1; $i <= 1000; $i++) {
            $hash = hash('sha512', $hash);
        }
        return $hash;
    }
}
