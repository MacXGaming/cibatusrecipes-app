<?php
class Icon{
    private $DB;

    public function __construct(){
        $this->DB = DB::getInstance();
    }

    public function get(int $iconId = 0){
        if($iconId==0){
            $query = $this->DB->query("SELECT * FROM icons ORDER BY name ASC")->results();
        }else{
            $query = $this->DB->query("SELECT * FROM icons WHERE id = ?", array($iconId))->results()[0];
        }
        return $query;
    }

    public function getByName($iconName){
        $query = $this->DB->query("SELECT * FROM icons WHERE name = ?", array($iconName))->results();
        return $query[0];
    }
    public function exist(int $iconId){
        $query = $this->DB->query("SELECT * FROM icons WHERE id = ?", array($iconId))->results();
        if($this->DB->count()>0){
            return true;
        }else{
            return false;
        }
    }
}
