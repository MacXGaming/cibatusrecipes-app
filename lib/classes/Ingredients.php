<?php
class Ingredients{
    private $DB;

    public function __construct(){
        $this->DB = DB::getInstance();
    }

    public function set(int $recipeId, $content){
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->DB->query("INSERT INTO recipe_ingredient (recipeId, name, ip)VALUES(?, ?, ?)", array($recipeId, $content, $ip));
        return true;
    }

    public function truncate(int $recipeId){
        $query = $this->DB->query("DELETE FROM recipe_ingredient WHERE recipeId = ?", array($recipeId));
        return true;
    }

    public function get(int $recipeId){
        if(empty($recipeId)){
            return false;
        }
        $query = $this->DB->query("SELECT * FROM recipe_ingredient WHERE recipeId = ?", array($recipeId))->results();
        return $query;
    }
}
