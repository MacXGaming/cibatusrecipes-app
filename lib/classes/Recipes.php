<?php
class Recipes{
    private $DB;
    private $Tags;
    private $Ingredients;

    public function __construct(){
        $this->DB = DB::getInstance();
        $this->Tags = new Tags();
        $this->Ingredients = new Ingredients();
    }

    public function add(int $userId, $title, array $ingredients, $description, $note, array $categories, array $tags){
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->DB->query("INSERT INTO recipes (userId, name, description, note, ip)VALUES(?, ?, ?, ?, ?)", array($userId, $title, $description, $note, $ip));
        $recipeId = $this->DB->lastId();
        foreach ($ingredients as $ingredient) {
            $this->Ingredients->set($recipeId, $ingredient);
        }
        foreach ($tags as $tag) {
            $this->Tags->set($recipeId, $tag);
        }

        foreach ($categories as $category) {
            $this->setCategory($recipeId, $category);
        }
        return true;
    }

    public function update(int $recipeId, int $userId, $title, array $ingredients, $description, $note, array $categories, array $tags){
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->DB->query("UPDATE recipes SET userId = ?, name = ?, description = ?, note = ?, ip = ? WHERE id = ?", array($userId, $title, $description, $note, $ip, $recipeId));

        $this->Ingredients->truncate($recipeId);
        foreach ($ingredients as $ingredient) {
            $this->Ingredients->set($recipeId, $ingredient);
        }
        $this->Tags->truncate($recipeId);
        foreach ($tags as $tag) {
            $this->Tags->set($recipeId, $tag);
        }
        $this->truncateCategory($recipeId);
        foreach ($categories as $category) {
            $this->setCategory($recipeId, $category);
        }
        return true;
    }

    public function delete(int $recipeId){
        $this->DB->query("DELETE FROM recipes WHERE id = ?", array($recipeId));
        $this->Ingredients->truncate($recipeId);
        $this->Tags->truncate($recipeId);
        $this->truncateCategory($recipeId);
        return true;
    }

    public function search(int $userId, $string){
        $query = $this->DB->query("SELECT * FROM recipes WHERE name LIKE ? AND userId = ? ORDER BY id DESC", array("%$string%", $userId))->results();
        return $query;
    }

    public function get(int $userId, int $recipeId = 0){
        if($recipeId==0){
            $query = $this->DB->query("SELECT * FROM recipes WHERE userId = ? ORDER BY id DESC", array($userId))->results();
        }else{
            $query = $this->DB->query("SELECT * FROM recipes WHERE userId = ? AND id = ? ORDER BY id DESC", array($userId, $recipeId))->results()[0];
        }
        return $query;
    }

    public function getRecipeCategories(int $recipeId){
        $cat = array();
        $query = $this->DB->query("SELECT * FROM recipe_category WHERE recipeId = ?", array($recipeId))->results();
        foreach ($query as $Category) {
            $cat[] = $this->DB->query("SELECT * FROM categories WHERE id = ?", array($Category->categoryId))->results()[0];
        }
        return $cat;
    }

    public function getByCategory(int $userId, int $categoryId){
        $query = $this->DB->query("SELECT * FROM recipe_category WHERE categoryId = ?", array($categoryId))->results();
        foreach ($query as $Category) {
            $cat[] = $this->DB->query("SELECT * FROM recipes WHERE id = ?", array($Category->recipeId))->results()[0];
        }
        return $cat;
    }

    public function getByCategorySlug(int $userId, $slug){
        $query = $this->DB->query("SELECT * FROM categories WHERE slug = ? AND userId = ?", array($slug, $userId))->results()[0];
        return $query;
    }


    public function getCategories(int $userId){
        $query = $this->DB->query("SELECT * FROM categories WHERE userId = ?", array($userId))->results();
        return $query;
    }

    public function existCategory(int $categoryId, int $userId){
        $query = $this->DB->query("SELECT * FROM categories WHERE id = ? AND userId = ?", array($categoryId, $userId))->results();
        if($this->DB->count()>0){
            return true;
        }else{
            return false;
        }
    }
    public function haveCategory(int $recipeId, int $categoryId){
        $query = $this->DB->query("SELECT * FROM recipe_category WHERE recipeId = ? AND categoryId = ?", array($recipeId, $categoryId))->results();
        if($this->DB->count()>0){
            return true;
        }else{
            return false;
        }
    }

    public function createCategory(int $userId, int $iconId, $name){
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->DB->query("INSERT INTO categories (userId, iconId, name, ip)VALUES(?, ?, ?, ?)", array($userId, $iconId, $name, $ip));
        $id = $this->DB->lastId();
        $slug = Basics::slugify("{$id}-{$name}");
        $this->DB->query("UPDATE categories SET slug = ? WHERE id = ?", array($slug, $id));
        return true;
    }

    public function editCategory(int $categoryId, int $userId, int $iconId, $name){
        $ip = $_SERVER['REMOTE_ADDR'];
        $slug = Basics::slugify("{$categoryId}-{$name}");
        $this->DB->query("UPDATE categories SET name = ?, slug = ?, iconId = ?, ip = ? WHERE id = ?", array($name, $slug, $iconId, $ip, $categoryId));
        return true;
    }

    public function deleteCategory(int $categoryId){
        $this->DB->query("DELETE FROM categories WHERE id = ?", array($categoryId));
        return true;
    }


    public function setCategory(int $recipeId, int $categoryId){
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->DB->query("INSERT INTO recipe_category (recipeId, categoryId, ip)VALUES(?, ?, ?)", array($recipeId, $categoryId, $ip));
        return true;
    }

    public function truncateCategory(int $recipeId){
        $query = $this->DB->query("DELETE FROM recipe_category WHERE recipeId = ?", array($recipeId));
        return true;
    }

}
