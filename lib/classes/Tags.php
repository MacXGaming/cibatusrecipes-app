<?php
class Tags{
    private $DB;

    public function __construct(){
        $this->DB = DB::getInstance();
    }

    public function set(int $recipeId, $tag){
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->DB->query("INSERT INTO recipe_tags (recipeId, name, ip)VALUES(?, ?, ?)", array($recipeId, $tag, $ip));
        return true;
    }

    public function truncate(int $recipeId){
        $query = $this->DB->query("DELETE FROM recipe_tags WHERE recipeId = ?", array($recipeId));
        return true;
    }

    public function get(int $recipeId){
        if(empty($recipeId)){
            return false;
        }
        $query = $this->DB->query("SELECT * FROM recipe_tags WHERE recipeId = ?", array($recipeId))->results();
        return $query;
    }

    public function getByName(int $userId, $name){
        $sql = "SELECT recipes.*, recipe_tags.name as tagName FROM recipe_tags LEFT JOIN recipes ON recipes.id = recipe_tags.recipeId WHERE recipe_tags.name = ? AND recipes.userId = ?";
        $query = $this->DB->query($sql, array($name, $userId))->results();

        return $query;
    }
}
