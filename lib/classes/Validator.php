<?php

class Validator{

    public static function Email($string){
        if (filter_var($string, FILTER_VALIDATE_EMAIL)) {
            return true;
        }else{
            return false;
        }
    }

    public static function URL($string){
        if (preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$string)) {
            return true;
        }else{
            return false;
        }
    }

    public static function OlderThan($date, $age){
        $from = new DateTime($date);
        $to   = new DateTime('today');
        $fromAge = $from->diff($to)->y;
        if($fromAge <= $age){
            return true;
        }else{
            return false;
        }
    }

    public static function getAgeFromDate($date){
        $from = new DateTime($date);
        $to   = new DateTime('today');
        return $from->diff($to)->y;
    }

    public static function IP($ip){
        if(filter_var($ip, FILTER_VALIDATE_IP)){
            return true;
        }else{
            return false;
        }
    }
}
