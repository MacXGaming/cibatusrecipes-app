<?php
session_start();

if($_SESSION['language']=="da_DK" OR $_SESSION['language']=="DK"){
    $language="da_DK.utf8";
}else{
    $language="en_US.utf8";
}

putenv("LANGUAGE=".$language);
putenv('LC_ALL='.$language);
setlocale(LC_ALL, $language);
$domain = 'messages';
bindtextdomain($domain, $_SERVER['DOCUMENT_ROOT']."/locale");
bind_textdomain_codeset($domain, 'UTF-8');
textdomain($domain);




/* MySQL settings */
define( 'DB_NAME',     'admin_cibatusrecipes' );
define( 'DB_USER',     'cibatusrecipes' );
define( 'DB_PASSWORD', '091297111' );
define( 'DB_HOST',     'localhost' );

date_default_timezone_set('Europe/Copenhagen');


$limit=10;
$page = 1;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
}
$limitT = (($page * $limit) - $limit);
$pagination = $limit;



$message="";
$data="";
$TagList="";
$CategoryList="";

require_once $_SERVER['DOCUMENT_ROOT'].'/lib/classes/DB.php';

spl_autoload_register(function ($class_name) {
    include $_SERVER['DOCUMENT_ROOT'].'/lib/classes/'. $class_name . '.php';
});



$Account = new Account();
$Recipes = new Recipes();
$Tags = new Tags();
$Ingredients = new Ingredients();
$Icon = new Icon();

if(isset($_SESSION['userId'])){
    $userId=$_SESSION['userId'];
    $AccountInfo = $Account->getAccountFromID($userId)[0];
}else{
    $userId=0;
}

function current_page_name() {
    return substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "") + 1);
}
