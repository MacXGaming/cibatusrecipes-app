<?php
$folder = basename(urldecode($_GET['folder']))."/";
$file = basename(urldecode($_GET['file']));
$fileDir = getcwd().'/../../../../private/';
if (file_exists($fileDir . $folder . $file)){
    $contents = file_get_contents($fileDir . $folder . $file);

    $filext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
    if($filext=="gif"){
        $end="gif";
    }elseif($filext=="png"){
        $end="png";
    }elseif($filext=="jpg" OR $filext=="jpeg"){
        $end="jpeg";
    }

    header('Content-type: image/'.$end);

    echo $contents;
}
