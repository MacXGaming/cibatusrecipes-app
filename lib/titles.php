<?php
include_once 'config.php';
$bigTitle = _("CibatusRecipes");
$pageTitle = current_page_name();

switch ($pageTitle) {
    case 'index.php':
    if(isset($_SESSION['recipeSearch'])){
        $pageTitle = _("Search").":".$_SESSION['recipeSearch'];
        $pageBody = "homepage searching";
    }else{
        $pageTitle = _("Homepage");
        $pageBody = "homepage";
    }
    break;
    case 'add-recipe.php':
    $pageTitle = _("Add Recipe");
    break;
    case 'recipe.php':
    if (isset($_GET['recipeId']) && !empty($_GET['recipeId'])) {
		$id = (INT) $_GET['recipeId'];
		$query = $Recipes->getRecipe($id, $userId)[0];
		$pageTitle = $query->name;
        $pageBody = "recipe-page";
	} else {
		$pageTitle = _("Recipe");
        $pageBody = "recipe-page";
	}
    break;
    case 'share.php':
    if (isset($_GET['id']) && !empty($_GET['id'])) {
		$id = (INT) $_GET['id'];
		$query = $Recipes->getRecipeBySharing($id)[0];
		$pageTitle = $query->name;
        $pageBody = "share";
	} else {
		$pageTitle = _("Recipe");
        $pageBody = "share";
	}
    break;
    case 'shared.php':
    if (isset($_GET['id']) && !empty($_GET['id'])) {
		$id = (INT) $_GET['id'];
		$query = $Recipes->getRecipe($id, 1, true)[0];
		$pageTitle = $query->name;
        $pageBody = "shared";
	} else {
		$pageTitle = _("Recipe");
        $pageBody = "shared";
	}
    break;
    case 'tag.php':
    if (isset($_GET['tag']) && !empty($_GET['tag'])) {
		$pageTitle = $_GET['tag'];
        $pageBody = "tag";
	} else {
		$pageTitle = _("Tag");
        $pageBody = "tag";
	}
    break;
    case 'settings/categories.php':
    $pageTitle = _("Category Settings");
    break;
    case 'settings/profile.php':
    $pageTitle = _("Change Profile Picture");
    break;
    case 'settings/settings.php':
    $pageTitle = _("Account Settings");
    break;
    case 'favorites.php':
    $pageTitle = _("Favorites");
    break;
}
//$pageTitle = $bigTitle . " &raquo; " . $pageTitle;
$pageTitle = $pageTitle . " | " . $bigTitle;
