<?php
include_once 'lib/config.php';
if(isset($_SESSION['userId'])){
    header("location:/");
}
if(isset($_POST['login'])){
    $email=$_POST['email'];
    $password=$Account->c_hash($_POST['password']);

    $getAccount = $Account->getAccountFromLogin($email, $password);
    if(!empty($getAccount)){
        $_SESSION['userId']=$getAccount[0]->id;
        $_SESSION['language'] = $getAccount[0]->language;
        $Account->changeLastOnline($getAccount[0]->id);
        Alert::addMessage(_("You are now logged in!"));
        Alert::setAlert("success");
        header("location:/");
        exit();
    }else{
        Alert::addMessage(_("Wrong email or password!"));
        Alert::setAlert("danger");
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <base href="https://<?php echo $_SERVER['HTTP_HOST']; ?>"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo _("Log in"); ?> | <?php echo _("CibatusRecipes"); ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="style/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="login-page skin-red">
    <div class="login-box">
        <div class="login-logo">
            <!-- <a href="/"><img src="/landing/img/logos/logo-lg.png"></a> -->
        </div>
        <div class="login-box-body">
            <p class="login-box-msg"><?php echo _("Log in to get started!"); ?></p>
            <form action="" method="post">
                <div class="form-group">
                    <input type="email" name="email" class="form-control" tabindex="1" placeholder="<?php echo _("Email"); ?>">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" tabindex="2" placeholder="<?php echo _("Password"); ?>">
                </div>
                <div class="col-8">
                    <a href="/create" tabindex="4"><?php echo _("Create Account"); ?></a><br>
                </div>
                <div class="col-8">
                    <a href="/forgot" tabindex="5"><?php echo _("Forgot your password?"); ?></a><br>
                </div>
                <div class="col-4">
                    <button type="submit" name="login" tabindex="3" class="full-width btn btn-primary btn-block btn-flat" style="margin-top:-16px;"><?php echo _("Login"); ?></button>
                </div>
                <div class="clear"></div>
            </form>
        </div>
    </div>

    <script src="js/jquery-1.12.4.js"></script>
    <?php
    $data .= "<div class=\"alert alert-info\" role=\"alert\"><span class=\"alert_text\">$message</span></div>\n";
    $data .= "<div class=\"alert alert-success\" role=\"alert\"><span class=\"alert_text\">$message</span></div>\n";
    $data .= "<div class=\"alert alert-warning\" role=\"alert\"><span class=\"alert_text\">$message</span></div>\n";
    $data .= "<div class=\"alert alert-danger\" role=\"alert\"><span class=\"alert_text\">$message</span></div>\n";
    echo $data;
    if (!empty($_SESSION['data_alert'])) {
        echo "<script type=\"text/javascript\">\n";
        echo "  $(document).ready(function() {\n";
            echo $_SESSION['data_alert'];
            echo "   $.ajax({\n";
                echo "     type: 'POST',\n";
                echo "     data: 'this=data_alert',\n";
                echo "     url: '/alertme',\n";
                echo "     cache: false,\n";
                echo "     async: false\n";
                echo "   });\n";
                echo "  });\n";
                echo "</script>\n";
            }
            ?>
        </body>
        </html>
