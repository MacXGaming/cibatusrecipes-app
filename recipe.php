<?php
include_once 'lib/config.php';

if($_SERVER['HTTP_HOST']=="localhost"){
    $dir_pics = $_SERVER['DOCUMENT_ROOT'].'/images/recipe/';
}else{
    $dir_pics = getcwd().'/../../../private/recipe/';
}

$recipeId = $_GET['recipeId'];

$recipe = $Recipes->get($userId, $recipeId);

if(empty($recipe)){
    header("location:/");
    exit();
}

$Notes=!empty($recipe->note);
$Pictures=true;

$recipeTitle = $recipe->name;
$recipeDescription = $recipe->description;

$recipeNotes = $recipe->note;




$categories = "";
$getCategories = $Recipes->getRecipeCategories($recipe->id);
$cnt=0;
foreach ($getCategories as $category) {
    $cnt++;
    if(count($getCategories)==$cnt){
        $categories .= "<a href=\"/category/{$category->slug}\">{$category->name}</a>\n";
    }else{
        $categories .= "<a href=\"/category/{$category->slug}\">{$category->name}</a>,\n";
    }
}
$ingredients = "";
$getIngredients = $Ingredients->get($recipe->id);
foreach ($getIngredients as $ingredient) {
    if(substr($ingredient->name, 0, 1)=="#"){
        $name = substr($ingredient->name, 1);
        $ingredients .= "<li style=\"list-style-type:none;margin-left:-20px;\"><h4 style=\"margin:0;margin-top:5px;font-style:italic;\">{$name}</h4></li>\n";
    }else{
        $ingredients .= "<li>{$ingredient->name}</li>\n";
    }
}
$tags = "";
$tagInput = "";
$getTags = $Tags->get($recipe->id);
$cnt=0;
foreach ($getTags as $tag) {
    $cnt++;
    if(count($getTags)==$cnt){
        $tags .= "<a href=\"/tag/{$tag->name}\">{$tag->name}</a>\n";
        $tagInput .= $tag->name;
    }else{
        $tags .= "<a href=\"/tag/{$tag->name}\">{$tag->name}</a>,\n";
        $tagInput .= "{$tag->name}, ";
    }
}




$shareCode = "http://".$_SERVER['HTTP_HOST']."/share/WEIRD_CODE_HERE";



if(isset($_POST['update'])){
    $title = $_POST['title'];
    $ingredients = $_POST['attributevalue'];
    $description = $_POST['description'];
    $note = $_POST['note'];
    $categories = $_POST['categories'];
    $tags = explode(",", strtolower($_POST['tags']));


    if(empty($title)){
        Alert::addMessage(_("What are you cooking? Tell me in the title please!"));
    }
    if(count($ingredients)==0){
        Alert::addMessage(_("A big bawl of air? Don\'t you need some ingredients?"));
    }
    if(count($categories)==0){
        Alert::addMessage(_("All disces need a category!"));
    }
    $wrongCat=0;
    foreach ($categories as $category) {
        if(!$Recipes->existCategory($category, $userId)){
            $wrongCat = 1;
        }
    }
    if($wrongCat==1){
        Alert::addMessage(_("One or more categories doesn\'t exist!"));
    }

    if(empty(Alert::getMessages())){
        $Recipes->update($recipeId, $userId, $title, $ingredients, $description, $note, $categories, $tags);
        Alert::addMessage(_("Your recipe is now updated!"));
        Alert::setAlert("success");
        header("refresh:0;");
        exit();
    }else{
        Alert::setAlert("danger");
    }

}


if(isset($_POST['delete'])){

    if($_POST['check']!=1){
        Alert::addMessage(_("You have to be sure to delete this recipe!"));
    }

    if(empty(Alert::getMessages())){
        $Recipes->delete($recipeId);
        Alert::addMessage(_("Your recipe is now deleted!"));
        Alert::setAlert("success");
        header("location:/");
        exit();
    }else{
        Alert::setAlert("danger");
    }
}

include_once 'header.php';
?>
<div class="content recipe-page">
    <div class="col-12">
        <div class="recipe">
            <h1><?php echo $recipeTitle; ?></h1>
            <div class="actions">
                <form method="post">
                    <ul>
                        <li data-toggle="tooltip" data-placement="left" title="Favorite"><button type="submit" name="favorite" class="btn btn-none"><i class="fa fa-star"></i></button></li>
                        <li data-toggle="tooltip" data-placement="left" title="Share"><button type="button" class="btn btn-none" data-toggle="modal" data-target="#share"><i class="fa fa-share-alt"></i></button></li>
                        <li data-toggle="tooltip" data-placement="left" title="Gallery"><button type="button" class="btn btn-none" data-toggle="modal" data-target="#gallery"><i class="fa fa-photo"></i></button></li>
                        <li data-toggle="tooltip" data-placement="left" title="Edit"><button type="button" class="btn btn-none" data-toggle="modal" data-target="#edit"><i class="fa fa-edit"></i></button></li>
                        <li data-toggle="tooltip" data-placement="left" title="Delete" ><button type="button" class="btn btn-none" data-toggle="modal" data-target="#delete"><i class="fa fa-trash"></i></button></li>
                    </ul>
                </form>
            </div>
            <div class="recipe-list">
                <ul>
                    <?php
                    echo $ingredients;
                    ?>
                </ul>
            </div>
            <div class="description">
                <?php echo nl2br($recipeDescription); ?>
            </div>
            <div class="tags">
                <label><?php echo _("Tags"); ?>:</label>
                <span><?php echo $tags; ?></span>
                <div class="clear"></div>
            </div>
            <div class="tags">
                <label><?php echo _("Categories"); ?>:</label>
                <span><?php echo $categories; ?></span>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <?php if($Notes){ ?>
        <div class="col-12">
            <div class="notes">
                <h1><?php echo _("Notes"); ?></h1>
                <div class="notes-text">
                    <?php echo nl2br($recipeNotes);?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <?php } ?>

        <?php if($Pictures!=FALSE){ ?>
            <div class="col-12">
                <div class="notes">
                    <h1><?php echo _("Gallery"); ?></h1>
                    <div class="notes-text">

                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>

    <div class="modal fade" id="share" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo _("Share");?>: <?php echo $recipeTitle;?></h4>
                </div>
                <div class="modal-body">
                    <form method="post">
                        <div class="form-group">
                            <label><?php echo _("Sharing Link"); ?></label>
                            <div class="input-group">
                                <input disabled class="form-control" value="<?php echo $shareCode;?>">
                                <span class="input-group-btn">
                                    <button name="resetLink" class="noEnterSubmit btn btn-default" type="submit"><i class="fa fa-refresh"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                    <form method="post">
                        <div class="form-group">
                            <h3><?php echo _("Share with another user"); ?></h3>
                            <div class="form-group">
                                <label><?php echo _("Email"); ?></label>
                                <div class="input-group">
                                    <input class="form-control" name="email" type="email">
                                    <span class="input-group-btn">
                                        <button name="share" class="noEnterSubmit btn btn-primary" type="submit">Share</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="col-6">
                        <div class="form-group">
                            <button onclick="Print(1)" class="full-width btn btn-default" type="button"><i class="fa fa-print"></i> <?php echo _("Print Without Notes"); ?></button>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <button onclick="Print(2)" class="full-width btn btn-default" type="button"><i class="fa fa-print"></i> <?php echo _("Print With Notes"); ?></button>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _("Close"); ?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo _("Add image to"); echo " ".$recipeTitle;?></h4>
                </div>
                <form method="post" id="form" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="edit-recipe">
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="file" name="image" class="form-control">
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _("Close"); ?></button>
                        <input type="submit" name="upload" class="noEnterSubmit btn btn-primary" value="<?php echo _("Upload"); ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo _("Edit"); ?>: <?php echo " ".$recipeTitle;?></h4>
                </div>
                <form method="post" id="form">
                    <div class="modal-body">
                        <div class="edit-recipe">
                            <div class="col-12">
                                <h1><input class="noEnterSubmit" type="text" name="title" class="title" placeholder="<?php echo _("Title"); ?>" value="<?php echo $recipeTitle; ?>"></h1>
                                <div class="ingredients-list form-group">
                                    <label><?php echo _("Add Ingredient"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="bottom" data-content="<?php echo _("Type one ingredient at a time, with the amount and press the tick.<br>The ingredients will be listed from first to last added ingredient.<br>If there's a mistake or an ingredient need a correction, add the corrected ingredient to the list, move it using the arrows, and delete the old one pressing the cross.<br><b>Start with a # and it will be a headline</b><br><span style='font-size:0.7rem;'>eg. 50 g chopped nuts, of choice</span>"); ?>"></i></label>
                                    <div class="attline">
                                        <div class="attval"><input type="text" name="attval[]" id="arrtibutevalue" class="noEnterSubmit" value=""></div>
                                        <div class="attvaladdl"><a href="javascript:void(0);" onclick="pladdline()" id="attlink"><i class="fa fa-check"></i></a></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="ingredients-list form-group">
                                    <label><?php echo _("Ingredients List"); ?></label>
                                    <ul class="ingredients" id="ingredients">
                                        <?php
                                        foreach ($getIngredients as $ingredient) {
                                            echo "            <li><input type=\"hidden\" name=\"attributevalue[]\" value=\"{$ingredient->name}\"><input disabled class=\"attributevalue\" value=\"{$ingredient->name}\"><a href=\"#\"><i class=\"fa fa-arrows-v\"></i></a><a href=\"javascript:void(0);\" onclick=\"$(this).parent().remove();\"><i class=\"fa fa-times\"></i></a></li>\n";
                                        }
                                        ?>
                                        <div class="clear"></div>
                                    </ul>

                                </div>
                                <div class="form-group">
                                    <label><?php echo _("Description"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="top" data-content="<?php echo _("Describe how to make the food.<br><span style='font-size:0.7rem;'>eg. Whip the eggs before slowly adding them to the mixture.<br>Baked for 20 mins. at 180 degrees.<br>Make sure the cake has been in the oven for at least 20 mins before opening. It can collapse!</span>"); ?>"></i></label>
                                    <textarea rows="10" name="description" class="text-field form-control"><?php echo $recipeDescription; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label><?php echo _("Note"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="top" data-content="<?php echo _("Note things you want to remember with this recipe.<br><span style='font-size:0.7rem;'>eg. Works well, served with sour cream.<br>Martin enjoys this very much.</span>"); ?>"></i></label>
                                    <textarea rows="10" name="note" class="text-field form-control"><?php echo $recipeNotes; ?></textarea>
                                </div>
                                <div class="category-list form-group">
                                    <label><?php echo _("Categories"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="top" data-content="<?php echo _("Choose one or more categories for the recipe. This will help finding the recipe again, even if you forget the title you give it."); ?>"></i></label>
                                    <select multiple required id="selectCategories" name="categories[]" class="form-control" style="height:200px;" id="countries" data-usesprite="smallIcons">
                                        <?php
                                        foreach ($Recipes->getCategories($userId) as $category) {
                                            $icon = $Icon->get($category->iconId);
                                            $selected="";
                                            if($Recipes->haveCategory($recipeId, $category->id)){
                                                $selected="selected";
                                            }
                                            echo "<option $selected value=\"{$category->id}\" data-icon=\"{$icon->class}\">{$category->name}</option>\n";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="tags form-group">
                                    <label><?php echo _("Tags"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="top" data-content="<?php echo _("Adding tags to a recipe, can make it easier to find it again. It can be ingredients, allergy-prone ingredients or the appropriate celebration, in which it's served.<br><span style='font-size:0.7rem;'>eg. christmas, nuts, gluten, chocolate</span>"); ?>"></i></label>
                                    <input type="text" name="tags" class="tags-input" value="<?php echo str_replace(' ', '', htmlentities($tagInput));?>" data-role="tagsinput">
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _("Close"); ?></button>
                        <input type="submit" name="update" class="noEnterSubmit btn btn-primary" value="<?php echo _("Save"); ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo _("Delete"); ?>: <?php echo " ".$recipeTitle;?></h4>
                </div>
                <form method="post">
                    <div class="modal-body">
                        <div class="remove-recipe">
                            <div class="col-12">
                                <div class="form-group">
                                    <input disabled type="text" class="form-control" value="<?php echo $recipeTitle;?>">
                                </div>
                                <label class="checkbox-inline"><input type="checkbox" name="check" value="1"><?php echo _("Are you sure to delete this?"); ?></label>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _("Close"); ?></button>
                        <input type="submit" name="delete" class="noEnterSubmit btn btn-danger" value="<?php echo _("Delete"); ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo _("Image"); ?></h4>
                </div>
                <form method="post">
                    <div class="modal-body">
                        <div class="remove-recipe">
                            <input type="number" name="id" class="id" style="display:none;">
                            <div class="col-12">
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _("Close"); ?></button>
                        <input type="submit" name="deleteImage" class="noEnterSubmit btn btn-danger" value="<?php echo _("Delete"); ?>">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="/js/bootstrap-tagsinput.js"></script>
    <script src="/js/jquery.multi-select.js"></script>
    <script>
    function openImage(url, id){
        $("#image .id").val(id);
        $("#image .col-12").html("<img style=\"width:100%;\" src=\""+url+"\">");
        $('#image').modal();
    }

    function Print(type){
        var left = (screen.width/2)-(600/2);
        var mywindow = window.open('/print/<?php echo 1;?>/' + type, null, 'height=400,width=600,left='+left);
        // mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        // mywindow.close();
        return true;
    }
    $(document).ready(function() {
        $('.noEnterSubmit').keypress(function(e){
            if ( e.which == 13 ) e.preventDefault();
        });
        $('#selectCategories').multiSelect({
            keepOrder: true,
            selectableHeader: "<div style='text-align:center;font-weight:bold;font-size:12px;'><?php echo _("Not Choosen"); ?></div>",
            selectionHeader: "<div style='text-align:center;font-weight:bold;font-size:12px;'><?php echo _("Choosen"); ?></div>"
        });


        $('[data-toggle="popover"]').popover({html:true});
        $('#attlink').hide();

        $('#arrtibutevalue').keydown(function (e){
            if(e.keyCode == 13){
                pladdline();
            }
        });
        $('#arrtibutevalue').keyup(function( event ) {
            if($('#arrtibutevalue').val().length>0){
                $('#attlink').fadeIn();
            }
            if($('#arrtibutevalue').val().length===0){
                $('#attlink').fadeOut();
            }
        });
    });
    function pladdline(){
        var err='';
        var title='';

        if($('#arrtibutevalue').val()==="") {

        }else{
            var pList='<li><input type="hidden" name="attributevalue[]" value="'+$('#arrtibutevalue').val()+'"><input disabled class="attributevalue" value="'+$('#arrtibutevalue').val()+'"><a href="#"><i class="fa fa-arrows-v"></i></a><a href="javascript:void(0);" onclick="$(this).parent().remove();"><i class="fa fa-times"></i></a></li>';
            $('#ingredients').append(pList);
        }
        $('#arrtibutevalue').val('');
        $('#attlink').fadeOut();
    }
    $('#ingredients').sortable();
    </script>
    <?php
    include_once 'footer.php';
    ?>
