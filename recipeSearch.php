<?php
include_once 'header.php';
$q = $_GET['q'];
?>
<div class="content homepage">
    <div class="col-12 recipe" style="position:relative;">
        <h1><?php echo _("Search"); ?>: <?php echo $q;?></h1>
    </div>
    <div class="col-12 recipe-list">
        <?php
        echo "<div class=\"list\">\n";
        foreach ($Recipes->search($userId, $q) as $recipe) {
            $categories = "";
            $getCategories = $Recipes->getRecipeCategories($recipe->id);
            $cnt=0;
            foreach ($getCategories as $category) {
                $cnt++;
                if(count($getCategories)==$cnt){
                    $categories .= "<a href=\"/category/{$category->slug}\">{$category->name}</a>\n";
                }else{
                    $categories .= "<a href=\"/category/{$category->slug}\">{$category->name}</a>,\n";
                }
            }
            $ingredients = "";
            foreach ($Ingredients->get($recipe->id) as $ingredient) {
                if(substr($ingredient->name, 0, 1)=="#"){
                    $name = substr($ingredient->name, 1);
                    $ingredients .= "<li style=\"list-style-type:none;margin-left:-20px;\"><h4 style=\"margin:0;margin-top:5px;font-style:italic;\">{$name}</h4></li>\n";
                }else{
                    $ingredients .= "<li>{$ingredient->name}</li>\n";
                }
            }
            $tags = "";
            $getTags = $Tags->get($recipe->id);
            $cnt=0;
            foreach ($getTags as $tag) {
                $cnt++;
                if(count($getTags)==$cnt){
                    $tags .= "<a href=\"/tag/{$tag->name}\">{$tag->name}</a>\n";
                }else{
                    $tags .= "<a href=\"/tag/{$tag->name}\">{$tag->name}</a>,\n";
                }
            }
            echo "<div class=\"recipe\">\n";
            echo "    <h1><a href=\"/recipe/{$recipe->id}\">{$recipe->name}</a></h1>\n";
            echo "    <div class=\"recipe-list\">\n";
            echo "        <ul>\n";
            echo "            {$ingredients}\n";
            echo "        </ul>\n";
            echo "    </div>\n";
            echo "    <div class=\"tags\">\n";
            echo "        <label>"._("Tags").":</label>\n";
            echo "        <span>{$tags}</span>\n";
            echo "        <div class=\"clear\"></div>\n";
            echo "    </div>\n";
            echo "    <div class=\"tags\">\n";
            echo "        <label>"._("Categories").":</label>\n";
            echo "        <span>{$categories}</span>\n";
            echo "        <div class=\"clear\"></div>\n";
            echo "    </div>\n";
            echo "</div>\n";
        }
        echo "</div>\n";
        ?>
        <div class="clear"></div>
    </div>
</div>
<script>
$(document).ready(function() {
    rePinterest();
});
function rePinterest(){
    $('.list').pinterest_grid({
        no_columns: 2,
        padding_x: 10,
        padding_y: 10,
        margin_bottom: 50,
        single_column_breakpoint: 1024
    });
}
</script>
<?php
include_once 'footer.php';
?>
