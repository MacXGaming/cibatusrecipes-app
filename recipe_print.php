<?php
include_once 'lib/config.php';

// if(count($Recipe)==0){
//     header("location:/");
//     exit();
// }

$Notes=true;

$recipeTitle = "Døds Pandekager";
$recipeDescription = "Whip eggs, sugar and vanilla sugar.
Put rest of the ingredients in the mix and stir together.
Pour into muffin cups.
<br><br>
Baked for 20 mins at 175 degrees.";

$recipeIngredient = array("Løg", "Is", "Eddike", "Citron");

$recipeNotes = "Loved by: René and Peter Dyhr.
<br><br><br>
Recipe from: Dorte Dyhr";


$Tags = array("død", "is", "sund");
$cntTag=0;
$TagInput="";
foreach ($Tags as $Tag) {
    $cntTag++;
    if($cntTag == count($Tags)){
        $TagList .= "<a href=\"/tag/{$Tag}\">".$Tag."</a>";
        $TagInput .= $Tag;
    }else{
        $TagList .= "<a href=\"/tag/{$Tag}\">".$Tag."</a>, ";
        $TagInput .= $Tag.", ";
    }
}

$Categories = array("Is", "Pandekager");
$cntCat=0;
foreach ($Categories as $Category) {
    $cntCat++;
    if($cntCat == count($Categories)){
        $CategoryList .= "<a href=\"/category/{$Category}\">{$Category}</a>";
    }else{
        $CategoryList .= "<a href=\"/category/{$Category}\">{$Category}</a>, ";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $recipeTitle;?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/style/css/style.css" title="no title">
    <style>body{background:#fff;}.recipe,.notes{border:0;padding:0;margin:0;}.notes{margin-top:25px;}</style>
</head>
<body>
    <div class="recipe">
        <h1><?php echo $recipeTitle;?></h1>
        <div class="recipe-list">
            <ul>
                <?php
                foreach ($recipeIngredient as $ingredient) {
                    echo "            <li>{$ingredient}</li>\n";
                }
                ?>
            </ul>
        </div>
        <div class="description">
            <?php echo $recipeDescription;?>
        </div>
        <div class="tags">
            <label><?php echo _("Tags"); ?>:</label>
            <span><?php echo $TagList;?></span>
            <div class="clear"></div>
        </div>
        <div class="tags">
            <label><?php echo _("Categories"); ?>:</label>
            <span><?php echo $CategoryList;?></span>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>

    <?php if($Notes AND $_GET['type']==2){ ?>
        <div class="col-12">
            <div class="notes">
                <h1><?php echo _("Notes"); ?></h1>
                <div class="notes-text">
                    <?php echo $recipeNotes;?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <?php } ?>
</body>
</html>
