<?php
include_once '../lib/config.php';
$modals="";

if(isset($_POST['add'])){
    $name = $_POST['name'];
    $iconId = $_POST['icon'];
    if(empty($name)){
        Alert::addMessage("You have to enter a name!");
    }
    if(!$Icon->exist($iconId)){
        Alert::addMessage("That icon doesn\'t exist!");
    }

    if(empty(Alert::getMessages())){
        $Recipes->createCategory($userId, $iconId, $name);
        Alert::addMessage("Your category is now created!");
        Alert::setAlert("success");
        header("refresh:0;");
        exit();
    }else{
        Alert::setAlert("danger");
    }
}

if(isset($_POST['edit'])){
    $categoryId = $_POST['categoryId'];
    $name = $_POST['name'];
    $iconId = $_POST['icon'];
    if(empty($name)){
        Alert::addMessage("You have to enter a name!");
    }
    if(!$Icon->exist($iconId)){
        Alert::addMessage("That icon doesn\'t exist!");
    }
    if(!$Recipes->existCategory($categoryId, $userId)){
        Alert::addMessage("That category doesn\'t exist!");
    }

    if(empty(Alert::getMessages())){
        $Recipes->editCategory($categoryId, $userId, $iconId, $name);
        Alert::addMessage("Your category is now updated!");
        Alert::setAlert("success");
        header("refresh:0;");
        exit();
    }else{
        Alert::setAlert("danger");
    }
}

if(isset($_POST['delete'])){
    $categoryId = $_POST['categoryId'];
    if(!$Recipes->existCategory($categoryId, $userId)){
        Alert::addMessage("That category doesn\'t exist!");
    }
    if($_POST['check']!=1){
        Alert::addMessage("You have to accept the removal!");
    }

    if(empty(Alert::getMessages())){
        $Recipes->deleteCategory($categoryId);
        Alert::addMessage("Your category is now deleted!");
        Alert::setAlert("success");
        header("refresh:0;");
        exit();
    }else{
        Alert::setAlert("danger");
    }
}

include_once '../header.php';
?>
<div class="content">
    <div class="col-12">
        <div class="settings-categories">
            <div class="panel-header">
                <h1><?php echo _("Categories"); ?></h1>
                <div class="actions">
                    <button href="#" class="btn btn-success" data-toggle="modal" data-target="#add"><?php echo _("Add"); ?></button>
                </div>
                <div class="clear"></div>
            </div>
            <div class="col-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?php echo _("Name"); ?></th>
                            <th><?php echo _("Icon"); ?></th>
                            <th style="width:80px;"><?php echo _("Actions"); ?></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        foreach ($Recipes->getCategories($userId) as $category) {
                            $icon = $Icon->get($category->iconId);
                            echo "<tr>\n";
                            echo "<td>{$category->name}</td>\n";
                            echo "<td><i class=\"icon icon-{$icon->class}\"></i></td>\n";
                            echo "<td>\n";
                            echo "<button class=\"btn btn-primary btn-sm\" data-toggle=\"modal\" data-target=\"#edit_{$category->id}\"><i class=\"fa fa-edit\"></i></button>\n";
                            echo "<button class=\"btn btn-danger btn-sm\" data-toggle=\"modal\" data-target=\"#delete_{$category->id}\"><i class=\"fa fa-trash\"></i></button>\n";
                            echo "</td>\n";
                            echo "</tr>\n";

                            $modals .= "<div class=\"modal fade\" id=\"edit_{$category->id}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">\n";
                            $modals .= "    <div class=\"modal-dialog modal-sm\" role=\"document\">\n";
                            $modals .= "        <div class=\"modal-content\">\n";
                            $modals .= "            <div class=\"modal-header\">\n";
                            $modals .= "                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n";
                            $modals .= "                <h4 class=\"modal-title\" id=\"myModalLabel\">"._("Edit")." - {$category->name}</h4>\n";
                            $modals .= "            </div>\n";
                            $modals .= "            <form method=\"post\" id=\"form\">\n";
                            $modals .= "                <input type=\"text\" name=\"categoryId\" value=\"{$category->id}\" style=\"display:none;\">\n";
                            $modals .= "                <div class=\"modal-body\">\n";
                            $modals .= "                    <div class=\"new-category\">\n";
                            $modals .= "                        <div class=\"col-12\">\n";
                            $modals .= "                            <div class=\"form-group\">\n";
                            $modals .= "                                <label>Name</label>\n";
                            $modals .= "                                <input type=\"text\" name=\"name\" value=\"{$category->name}\" class=\"form-control noEnterSubmit\">\n";
                            $modals .= "                            </div>\n";
                            $modals .= "                            <div class=\"form-group\">\n";
                            $modals .= "                                <label>"._("Icon")." <i class=\"fa fa-info-circle\" title=\""._("Help")."\" data-toggle=\"popover\" data-placement=\"top\" data-content=\""._("Choose whatever icon you feel like fits the category, from the list of possible icons.<br>The icon chosen will be shown under the list of categories, as well as in the topbar/category-bar. This icon can be changed at any time.")."\"></i></label>\n";
                            $modals .= "                                <select name=\"icon\" class=\"selectpicker form-control noEnterSubmit\">\n";
                            foreach ($Icon->get() as $icon) {
                                $selected="";
                                if($category->iconId==$icon->id){
                                    $selected="selected";
                                }
                                $modals .= "<option {$selected} data-icon=\"icon-{$icon->class}\" value=\"{$icon->id}\">{$icon->name}</option>\n";
                            }
                            $modals .= "                                </select>\n";
                            $modals .= "                            </div>\n";
                            $modals .= "                            <div class=\"clear\"></div>\n";
                            $modals .= "                        </div>\n";
                            $modals .= "                        <div class=\"clear\"></div>\n";
                            $modals .= "                    </div>\n";
                            $modals .= "                </div>\n";
                            $modals .= "                <div class=\"modal-footer\">\n";
                            $modals .= "                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">".("Close")."</button>\n";
                            $modals .= "                    <input type=\"submit\" name=\"edit\" class=\"noEnterSubmit btn btn-primary\" value=\""._("Edit")."\">\n";
                            $modals .= "                </div>\n";
                            $modals .= "            </form>\n";
                            $modals .= "        </div>\n";
                            $modals .= "    </div>\n";
                            $modals .= "</div>\n";

                            $modals .= "<div class=\"modal fade\" id=\"delete_{$category->id}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">\n";
                            $modals .= "    <div class=\"modal-dialog modal-sm\" role=\"document\">\n";
                            $modals .= "        <div class=\"modal-content\">\n";
                            $modals .= "            <div class=\"modal-header\">\n";
                            $modals .= "                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n";
                            $modals .= "                <h4 class=\"modal-title\" id=\"myModalLabel\">"._("Delete")." - {$category->name}</h4>\n";
                            $modals .= "            </div>\n";
                            $modals .= "            <form method=\"post\" id=\"form\">\n";
                            $modals .= "                <input type=\"text\" name=\"categoryId\" value=\"{$category->id}\" style=\"display:none;\">\n";
                            $modals .= "                <div class=\"modal-body\">\n";
                            $modals .= "                    <div class=\"delete-category\">\n";
                            $modals .= "                        <div class=\"col-12\">\n";
                            $modals .= "                            <div class=\"form-group\">\n";
                            $modals .= "                                <label>"._("Name")."</label>\n";
                            $modals .= "                                <input disabled type=\"text\" name=\"name\" value=\"{$category->name}\" class=\"form-control noEnterSubmit\">\n";
                            $modals .= "                            </div>\n";
                            $modals .= "                            <div class=\"form-group\">\n";
                            $modals .= "                                <label class=\"checkbox-inline\"><input type=\"checkbox\" name=\"check\" value=\"1\">"._("Are you sure to delete this?")."</label>\n";
                            $modals .= "                            </div>\n";
                            $modals .= "                            <div class=\"clear\"></div>\n";
                            $modals .= "                        </div>\n";
                            $modals .= "                        <div class=\"clear\"></div>\n";
                            $modals .= "                    </div>\n";
                            $modals .= "                </div>\n";
                            $modals .= "                <div class=\"modal-footer\">\n";
                            $modals .= "                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">"._("Close")."</button>\n";
                            $modals .= "                    <input type=\"submit\" name=\"delete\" class=\"noEnterSubmit btn btn-danger\" value=\""._("Delete")."\">\n";
                            $modals .= "                </div>\n";
                            $modals .= "            </form>\n";
                            $modals .= "        </div>\n";
                            $modals .= "    </div>\n";
                            $modals .= "</div>\n";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>


<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo _("Add Category"); ?></h4>
            </div>
            <form method="post" id="form">
                <div class="modal-body">
                    <div class="new-category">
                        <div class="col-12">
                            <div class="form-group">
                                <label><?php echo _("Name"); ?></label>
                                <input type="text" name="name" class="form-control noEnterSubmit">
                            </div>
                            <div class="form-group">
                                <label><?php echo _("Icon"); ?> <i class="fa fa-info-circle" title="<?php echo _("Help"); ?>" data-toggle="popover" data-placement="top" data-content="<?php echo _("Choose whatever icon you feel like fits the category, from the list of possible icons.<br>The icon chosen will be shown under the list of categories, as well as in the topbar/category-bar. This icon can be changed at any time.");?>"></i></label>
                                <select name="icon" class="selectpicker form-control noEnterSubmit">
                                    <?php
                                    foreach ($Icon->get() as $icon) {
                                        echo "<option data-icon=\"icon-{$icon->class}\" value=\"{$icon->id}\">{$icon->name}</option>\n";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _("Close"); ?></button>
                    <input type="submit" name="add" class="noEnterSubmit btn btn-primary" value="<?php echo _("Add"); ?>">
                </div>
            </form>
        </div>
    </div>
</div>
<?php
echo $modals;
?>

<script src="/js/jquery.select.js"></script>
<script>
$(document).ready(function() {
    $('[data-toggle="popover"]').popover({html:true});
    $('#attlink').hide();
    $('.noEnterSubmit').keypress(function(e){
        if ( e.which == 13 ) e.preventDefault();
    });
});
</script>
<?php
include_once '../footer.php';
?>
