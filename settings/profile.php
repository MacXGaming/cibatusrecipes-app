<?php
include_once '../lib/config.php';
if(isset($_POST['update'])){

    if($_SERVER['HTTP_HOST']=="localhost"){
        $dir_pics = $_SERVER['DOCUMENT_ROOT'].'/images/profile/';
    }else{
        $dir_pics = getcwd().'/../../../../private/profile/';
    }

    $old_image = explode("_", $AccountInfo->picture);
    $time = time();


    $Upload = new Upload($_FILES['image']);
    $Upload->setMaxSize(32, "MB");
    $Upload->AllowedTypes(array("image/png", "image/gif", "image/jpeg"));

    $Upload->setName($userId);
    $Upload->setPrefix("org_");
    $Upload->setSuffix("_".$time);
    $Upload->setPath($dir_pics);
    $Upload->Render();

    $Upload->setName($userId);
    $Upload->setPrefix("thumb_");
    $Upload->setSuffix("_".$time);
    $Upload->setPath($dir_pics);
    $Upload->setWidth(160);
    $path = $Upload->Render();

    $Upload->Clean();

    foreach ($Upload->errors() as $error) {
        $errors[] = $error;
    }

    if(empty($errors)){
        unlink($dir_pics."org_".$old_image[1]."_".$old_image[2]);
        unlink($dir_pics."thumb_".$old_image[1]."_".$old_image[2]);
        $Account->updatePicture($userId, $path);
        Alert::addAlert("success", array(_("Your picture is now updated!")));
        header("location:/settings/profile");
    }else{
        Alert::addAlert("danger", $errors);
    }
}

include_once '../header.php';
?>
<div class="content">
    <div class="col-12">
        <div class="settings-account">
            <div class="panel-header">
                <h1><?php echo _("Profile Picture"); ?></h1>
                <div class="clear"></div>
            </div>
            <form method="post" enctype="multipart/form-data">
                <div class="col-6 padding-5">
                    <div class="form-group">
                        <label><?php echo _("Picture"); ?></label>
                        <input class="form-control" type="file" name="image">
                    </div>
                </div>
                <div class="col-12 padding-5">
                    <input name="update" type="submit" class="btn btn-success" value="<?php echo _("Update"); ?>">
                </div>
            </form>
            <div class="clear"></div>
        </div>
    </div>
</div>

<?php
include_once '../footer.php';
?>
