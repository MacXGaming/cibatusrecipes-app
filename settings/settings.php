<?php
include_once '../lib/config.php';

if(isset($_POST['update'])){
    $name = $_POST['name'];
    $email = $_POST['email'];
    $language = $_POST['language'];

    $old_pass = $_POST['old-pass'];
    $new_pass = $_POST['new-pass'];
    $rep_new_pass = $_POST['rep-new-pass'];

    if(empty($name)){
        $errors[] = _("Tell me your name please!");
    }
    if(empty($email)){
        $errors[] = _("How can you log in without an email?");
    }
    if(empty($language)){
        $errors[] = _("You have to choose a primary language?");
    }

    if(!empty($old_pass)){
        if($Account->c_hash($old_pass)!=$AccountInfo->password){
            $errors[] = _("That is not your old password!");
        }
        if($new_pass!=$rep_new_pass){
            $errors[] = _("Your new password doesn\'t match!");
        }
    }

    if(empty($errors)){
        $Account->updateAccount($userId, $name, $email, $language);
        if(!empty($old_pass)){
            $Account->updatePassword($userId, $new_pass);
        }
        Alert::setAlert("success", array(_("Your account is now updated!")));
        header("location:/settings/account");
        exit();
    }else{
        Alert::setAlert("danger", $errors);
    }
}

include_once '../header.php';
?>
<div class="content">
    <div class="col-12">
        <div class="settings-account">
            <div class="panel-header">
                <h1><?php echo _("Settings"); ?></h1>
                <div class="clear"></div>
            </div>
            <form method="post">
                <div class="col-6 padding-5">
                    <h2><?php echo _("Account"); ?></h2>
                    <div class="form-group">
                        <label><?php echo _("Name"); ?></label>
                        <input class="form-control" type="text" name="name" value="<?php echo $AccountInfo->name;?>">
                    </div>
                    <div class="form-group">
                        <label><?php echo _("Email"); ?></label>
                        <input class="form-control" type="email" name="email" value="<?php echo $AccountInfo->email;?>">
                    </div>
                    <div class="form-group">
                        <label><?php echo _("Language"); ?></label>
                        <select name="language" class="form-control form-select" style="border-radius: 0; -webkit-appearance: none; -moz-appearance: none;">
                            <option value=""><?php echo _("Choose primary language"); ?></option>
                            <option value="DK" <?php if($AccountInfo->language == "DK"){ echo "selected"; } ?>>Danish</option>
                            <option value="US" <?php if($AccountInfo->language == "US"){ echo "selected"; } ?>>English</option>
                        </select>
                    </div>
                </div>
                <div class="col-6 padding-5">
                    <h2><?php echo _("Password <span>(Only fill if you want to change!)</span>"); ?></h2>
                    <div class="form-group">
                        <label><?php echo _("Old Password"); ?></label>
                        <input class="form-control" type="password" name="old-pass">
                    </div>
                    <div class="form-group">
                        <label><?php echo _("New Password"); ?></label>
                        <input class="form-control" type="password" name="new-pass">
                    </div>
                    <div class="form-group">
                        <label><?php echo _("Repeat New Password"); ?></label>
                        <input class="form-control" type="password" name="rep-new-pass">
                    </div>
                </div>
                <div class="col-12 padding-5">
                    <input name="update" type="submit" class="btn btn-success" value="<?php echo _("Update"); ?>">
                </div>
            </form>
            <div class="clear"></div>
        </div>
    </div>
</div>

<?php
include_once '../footer.php';
?>
